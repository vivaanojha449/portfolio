import Navbar from './components/Header';
import Profile from './components/Profile';
import Footer from './components/Footer';
import Portfolio from './components/Project';
import Home from './components/Home';
import Skill from './components/Skill';
import Experience from './components/Experience';

function App() {
  return (
    <>  <Navbar />
      <Home />
      <Profile />
      <Portfolio />
      <Experience />
      <Skill />
      <Footer />
    </>
  );
}

export default App;
