import React from 'react';
import { Box, Flex, Heading, Tooltip } from '@chakra-ui/react';
import { SiHtml5, SiCss3, SiReact, SiJavascript, SiSass, SiBootstrap, SiMongodb, SiExpress } from 'react-icons/si';
import Image from './assets/codeone.avif';
import { FaNodeJs } from 'react-icons/fa';


const Skill = () => {
  return (
    <Box
      p="8"
      position="relative"
      minHeight="500px"
      backgroundImage={Image} 
      backgroundSize="cover"
      backgroundPosition="center"
      backgroundRepeat="no-repeat"
      id='skill'
    >
      <Heading as="h2" size="xl" mt="40" mb="10" textAlign="center" color="white">SKILLS</Heading>
      <Flex flexWrap="wrap" justifyContent="center">
        <SkillItem icon={<SiHtml5 />} name="HTML5" delay={0} />
        <SkillItem icon={<SiCss3 />} name="CSS3" delay={0.2} />
        <SkillItem icon={<SiReact />} name="React" delay={0.4} />
        <SkillItem icon={<SiJavascript />} name="JavaScript" delay={0.6} />
        <SkillItem icon={<SiSass />} name="Sass" delay={0.8} />
        <SkillItem icon={<SiBootstrap />} name="Bootstrap" delay={1} />
        <SkillItem icon={<SiMongodb />} name="MongoDB" delay={1.2} />
        <SkillItem icon={<SiExpress />} name="Express.js" delay={1.4} />
        <SkillItem icon={<FaNodeJs />} name="Node.js" delay={1.6} />

      </Flex>
    </Box>
  );
};

const SkillItem = ({ icon, name, delay }) => {
  const style = {
    transition: `transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55) ${delay}s`,
  };

  return (
    <Tooltip label={name} aria-label={name} placement="top">
      <Box p="4" textAlign="center" style={style} color="white" display="flex" flexDirection="column" alignItems="center">
        <div style={{ width: '100%', display: 'flex', justifyContent: 'center'}}>
          {icon}
        </div>
        <Box mt="2">{name}</Box>
      </Box>
    </Tooltip>
  );
};

export default Skill;
