import React from 'react';
import { Box, Grid, GridItem, Text, Image, useColorMode } from "@chakra-ui/react";
import circleImage1 from './assets/three.jpg';
import { Switch } from '@chakra-ui/react';

function Profile() {
    const { colorMode, toggleColorMode } = useColorMode();

    const isDark = colorMode === 'dark';

    const handleToggle = () => {
        toggleColorMode();
    };

    return (
        <Grid templateColumns={{ base: "2fr", md: "50% 50%" }} gap="4" order={[1, 2]} id='about'>
            {/* Main Content */}
            <GridItem colSpan={1}>
            <Box textAlign="center">
    <Text fontSize="2xl" mt="50" ml={{base: "0", md: "75px"}} fontWeight="bold">About Me</Text>
    <Text mt="15" ml={{base: "0", md: "75px"}} mr={{base: "0", md: "75px"}}>
        I'm a Full-stack developer adept in React, Express, MongoDB, and related technologies. Excels in designing intuitive user interfaces, building scalable server-side applications, and implementing efficient data solutions. Passionate about crafting seamless user experiences and keen on exploring emerging technologies. Experienced in project design and deployment, with a focus on optimizing development workflows for enhanced productivity.
    </Text>
</Box>

<Box textAlign={{base: "center", md: "center"}}>
    <Text fontSize="2xl" mt="10" ml={{base:"0", md:"75px"}} fontWeight="bold">Contact details</Text>
    <Box ml={{base:"0", md:"75px"}}>
        <Text ml={{base:"0", md:"0"}}>Vivaan Ojha</Text>
        <Text>A1-601 Mohan Park Kalyan (West)</Text>
        <a href="tel:+918097690289"><Text ml={{base:"0", md:"0"}}>+91-8097690289</Text></a>
        <a href="mailto:vivaanojha449@gmail.com"><Text ml={{base:"0", md:"0"}}>vivaaanojha449@gmail.com</Text></a>
    </Box>
</Box>

                <Switch
                    isChecked={isDark}
                    onChange={handleToggle}
                    colorScheme="teal"
                    size="lg"
                    mt="50"
                    ml={{base:"160", md:"385"}}
                />
            </GridItem>

           {/* Developer Cartoon Image */}
{/* Developer Cartoon Image */}
<GridItem
  mt="30px"
  mb="20"
  height="50vh"
  display="flex"
  justifyContent="center"
  alignItems="center"
> {/* Adjust height as needed */}
  <Image
    src={circleImage1}
    alt="Portfolio Image 1"
    borderRadius="full"
    width="auto"
    height="auto"
    maxW="100%"
    maxH="100%"
    mt="20px"
    p="25px"
  />
</GridItem>


        </Grid>
    );
}

export default Profile;
