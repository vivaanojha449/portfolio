import { Link as ScrollLink } from "react-scroll";
import React, { useState } from 'react';
import { Box, Flex, Spacer, Link, IconButton, useColorMode, Image } from "@chakra-ui/react";
import { HamburgerIcon, SunIcon, MoonIcon } from "@chakra-ui/icons";
import { FaUser, FaBriefcase, FaFileAlt, FaTools } from 'react-icons/fa';
import profile from "./assets/img2.jpg";

function Navbar() {
  const { colorMode, toggleColorMode } = useColorMode();
  const [showMenu, setShowMenu] = useState(false);

  const handleMenuToggle = () => {
    setShowMenu(!showMenu);
  };

  return (
    <Flex p="4" bg="black" color="white" alignItems="center" justifyContent="space-between">
      <Box>
        <Image src={profile} borderRadius="full" boxSize="40px" mr="4" />
      </Box>
      <Box>
       <Link className="nav-item" _hover={{ textDecoration: 'none', color: 'gray' }} href="Certificate.pdf" target='_blank' download="Certificate.pdf" fontSize="xl" fontWeight="bold" color="#319795">
          Certificate
        </Link>
        {/* href="Vivaan_CV.pdf" target='_blank' download="Certificate.pdf" */}
      </Box>
      <Spacer />
      <Box display={{ base: "none", md: "flex" }}>
        <ScrollLink to="about" smooth={true} duration={500}>
           <Link href="/about" mr="4" color="white" _hover={{ textDecoration: 'none', color: '#319795' }}  style={{ fontFamily: 'Arial, sans-serif' }} >
       <Box ml="12px"><FaUser/></Box>
          <span className="nav-item" style={{ fontFamily: 'Arial, sans-serif' }}>About</span>
        </Link></ScrollLink>
        <ScrollLink to='project' smooth={true} duration={500}>
          <Link href="/portfolio" mr="4" color="white" _hover={{ textDecoration: 'none', color: '#319795' }} style={{ fontFamily: 'Arial, sans-serif' }}>
        <Box ml="18px">  <FaBriefcase /></Box>
          <span className="nav-item" style={{ fontFamily: 'Arial, sans-serif' }}>Project</span>
        </Link></ScrollLink>
        <ScrollLink to="experience" smooth={true} duration={500}>
          <Link href="experience" mr="4" color="white" _hover={{ textDecoration: 'none', color: '#319795' }} style={{ fontFamily: 'Arial, sans-serif' }}>
          <Box ml="22px"><FaTools/></Box> 
            <span className="nav-item" style={{ fontFamily: 'Arial, sans-serif' }}>Expertise</span>
          </Link>
        </ScrollLink>
        <ScrollLink to='skill' smooth={true} duration={500}>
           <Link href="" mr="4" color="white" _hover={{ textDecoration: 'none', color: '#319795' }} style={{ fontFamily: 'Arial, sans-serif' }}>
           <Box ml="8px"><FaFileAlt />   </Box> 
          <span className="nav-item" style={{ fontFamily: 'Arial, sans-serif' }}>Skills</span>
        </Link></ScrollLink>
      </Box>
      <IconButton
        icon={colorMode === "dark" ? <SunIcon /> : <MoonIcon />}
        onClick={toggleColorMode}
        aria-label="Toggle color mode"
        color="white"
        variant="ghost"
      />
      <IconButton
        display={{ base: "flex", md: "none" }}
        icon={<HamburgerIcon />}
        aria-label="Toggle menu"
        color="white"
        variant="ghost"
        onClick={handleMenuToggle}
      />
      {showMenu && (
        <Box display={{ base: "flex", md: "none" }} flexDirection="column" bg="black" color="white" mt="4">
          <ScrollLink to='about' smooth={true} duration={500}><Link href="/about" mb="2" _hover={{ textDecoration: 'none', color: '#808080' }} style={{ fontFamily: 'Arial, sans-serif' }}>
            About
          </Link></ScrollLink>
          <ScrollLink to='project' smooth={true} duration={500}><Link href="/portfolio" mb="2" _hover={{ textDecoration: 'none', color: '#808080' }} style={{ fontFamily: 'Arial, sans-serif' }}>
            Project
          </Link></ScrollLink>
          <ScrollLink to='experience' smooth={true} duration={500}><Link href="/resume" mb="2" _hover={{ textDecoration: 'none', color: '#808080' }} style={{ fontFamily: 'Arial, sans-serif' }}>
          Expertise
          </Link></ScrollLink>
          <ScrollLink to='skill' smooth={true} duration={500}><Link href="/contact" mb="2" _hover={{ textDecoration: 'none', color: '#808080' }} style={{ fontFamily: 'Arial, sans-serif' }}>
            Skills
          </Link></ScrollLink>
          
          
          
          
        </Box>
      )}
    </Flex>
  );
}

export default Navbar;
