import React from 'react';
import { Box, Flex, Heading, Text, Button, Icon } from '@chakra-ui/react';
import { FaGitlab, FaLinkedin } from 'react-icons/fa';
import { FiMail, FiPhone, FiChevronDown } from 'react-icons/fi';
import Background from './assets/one.jpg'

const Home = () => {

  return (
    <Box
      bgImage={Background}
      bgSize="cover"
      bgPosition="center"
      bgRepeat="no-repeat"
      h="100vh"
      position="relative"
      // Added the following styles to ensure full width
      left="0"
      right="0"
      width="100%"
    >
      <Flex
        direction="column"
        justify="center"
        align="center"
        h="100%"
        color="white"
        textAlign="center"
        textShadow="2px 2px 4px rgba(0,0,0,0.5)"
      >
        <Heading as="h1" size="4xl" fontWeight="bold" mb="4" _hover={{ textDecoration: 'none', color: '#319795 '}} className="nav-item" >
          Vivaan Ojha
        </Heading>
        <Text fontSize="2xl" mb="8" fontFamily={"Figtree,sansarif"} _hover={{ textDecoration: 'none', color: '#319795'}} className="nav-item">
          Full Stack Developer
        </Text>
        <Flex align="center" justify="center" mb="8">
          <a href="https://gitlab.com/vivaanojha449" target="_blank" rel="noopener noreferrer" >
            <Icon as={FaGitlab} boxSize="10" mr="4" _hover={{ textDecoration: 'none', color: 'orange'}} className="nav-item"/>
          </a>
          <a href="https://linkedin.com/in/vivaan-ojha-23b850136" target="_blank" rel="noopener noreferrer">
            <Icon as={FaLinkedin} boxSize="10" mr="4" _hover={{ textDecoration: 'none', color: 'blue.500'}} className="nav-item"/>
          </a>
          <a href="mailto:vivaanojha449@gmail.com">
            <Icon as={FiMail} boxSize="10" mr="4" _hover={{ textDecoration: 'none', color: 'red'}} className="nav-item"/>
          </a>
          <a href="tel:+918097690289">
            <Icon as={FiPhone} boxSize="10" mr="4" _hover={{ textDecoration: 'none', color: 'violet'}} className="nav-item"/>
          </a>
          {/* <Text>+91 8097690289</Text> */}
        </Flex>
        <Button
          variant="outline"
          color="white"
          borderColor="white"
          borderRadius="full"
          size="lg"
          aria-label="Scroll down"
          _hover={{ textDecoration: 'none', color: '#319795'}}
          className="nav-item"
          onClick={() => {
            window.scrollTo({
              top: window.innerHeight,
              behavior: 'smooth',
              
            });
          }}
        >
          <Icon as={FiChevronDown} boxSize="8" _hover={{ textDecoration: 'none', color: 'black'}} className="nav-item" />
        </Button >
       <a href="Vivaan_CV.pdf" target='_blank' download="Vivaan_CV.pdf">
       <Button bg="black" _hover={{ bg: '#319795' }} color="white" mt="10" ml="auto">
          Download Resume
        </Button>
       </a>

      </Flex>

    </Box>
  );
};

export default Home;
