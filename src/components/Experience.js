import { Box, Flex, Grid, Text } from "@chakra-ui/react";

const Experience = () => {
  return (
   <Grid ml={{base:"1rem",md:"0"}} mr={{base:"1rem",md:"0"}} bg="white" mb={{base:"4rem",md:"0rem"}} mt={{base:"4rem",md:"8rem"}} id="experience">
    {/* <GridItem>
  
    </GridItem> */}
    <Box bg="white" p={4} minHeight="250px" ml={{base:"2rem",md:"0"}} mt={{base:"1rem",md:"9rem"}} mr={{base:"2rem",md:"0"}} display={{base:"flex"}} flexDirection={{base:"column", md:"row"}}>
      {/* <Flex mt={4}>
      </Flex> */}
        <Flex direction="column" width={{base:"50%", md:"40%"}} pr={4}>
          <Text fontSize="xl" fontWeight="bold" color="black" ml={{base:"0px", md:"50px" }} mt="25px">Education</Text>
        </Flex>
        <Flex direction="column"  width={{base:"50%", md:"60%"}}>
          <Text fontSize="lg" color="black">University of Mumbai</Text>
          <Text fontSize="md" mt="4"  color="gray.500" fontFamily={"Courgette,sansarif"}><Flex>Master in Information Technology <li style={{marginLeft:"30px", fontFamily:"Figtree"}}>May 2024</li></Flex></Text>
          <Text fontSize="sm" mt="3"  color="gray.400">Graduated with Distinction</Text>
        </Flex>
    </Box>
    {/* <GridItem>
    </GridItem> */}
      <Box bg="white" p={4} minHeight="250px" ml={{base:"2rem",md:"0"}} mr={{base:"2rem",md:"0"}} mb={{base:"2rem",md:"0"}} display={{base:"flex"}} flexDirection={{base:"column", md:"row"}}>
      {/* <Flex mt={4}>
      </Flex> */}
        <Flex direction="column" width={{base:"50%", md:"40%"}} pr={4}>
          <Text fontSize="xl" fontWeight="bold" color="black" ml={{base:"px", md:"50px" }} mt="25px">Work</Text>
        </Flex>
        <Flex direction="column"  width={{base:"50%", md:"60%"}}>
          <Text fontSize="lg" color="black">Accenture</Text>
          <Text fontSize="md" mt="4" color="gray.500"  fontFamily={"Courgette,sansarif"}><Flex>Web Developer <li style={{marginLeft:"20px", fontFamily:"Figtree"}}>May 2024 Present</li></Flex></Text>
          <Text fontSize="sm" mt="3" color="gray.400">Front-end developer</Text>
        </Flex>
    </Box>  
   </Grid>
    
  );
};

export default Experience;
