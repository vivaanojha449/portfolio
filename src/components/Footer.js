import React, { useState, useEffect } from 'react';
import { Box, Flex, Icon, Text, Heading } from '@chakra-ui/react';
import { FaGitlab, FaLinkedin, FaEnvelope, FaPhone, FaArrowCircleUp } from 'react-icons/fa';

const Footer = () => {
  const [showScroll, setShowScroll] = useState(false);

  useEffect(() => {
    const checkScrollTop = () => {
      if (!showScroll && window.scrollY > 400) {
        setShowScroll(true);
      } else if (showScroll && window.scrollY <= 400) {
        setShowScroll(false);
      }
    };

    window.addEventListener('scroll', checkScrollTop);
    return () => {
      window.removeEventListener('scroll', checkScrollTop);
    };
  }, [showScroll]);

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <Flex
      as="footer"
      align="center"
      justify="center"
      bg="black"
      color="white"
      py="4"
      px={{ base: '4', md: '15%' }}
      position="relative"
      bottom="0"
      left="0"
      width="100%"
      zIndex="999"
    >
      <Box textAlign="center" ml={{ base: "auto", md: "auto" }} mr={{ base: "auto", md: "auto" }}>
        <Box display={{ base: "flex", md: "block" }} ml={{ base: "25px", md: "0" }}>
          <a href="https://gitlab.com/vivaanojha449" target="_blank" rel="noopener noreferrer">
            <Icon as={FaGitlab} boxSize="6" mx="2" _hover={{ textDecoration: 'none', color: 'orange'}} className="nav-item" /></a>
          <a href="https://linkedin.com/in/vivaan-ojha-23b850136" target="_blank" rel="noopener noreferrer">
            <Icon as={FaLinkedin} boxSize="6" mx="2" _hover={{ textDecoration: 'none', color: 'blue.500'}} className="nav-item"  />
          </a>
          <a href="mailto:vivaanojha449@gmail.com">
            <Icon as={FaEnvelope} boxSize="6" mx="2" _hover={{ textDecoration: 'none', color: 'red'}} className="nav-item" />
          </a>
          <a href="tel:+918097690289">
            <Icon as={FaPhone} boxSize="6" mx="2" _hover={{ textDecoration: 'none', color: 'violet'}} className="nav-item" />
          </a>      </Box>
        <Text mt="3">Designed by Vivaan</Text>
        <Heading mt={'3'} size='sm' textAlign={'center'} color={'teal.500'}><Text>All rights reserved ©️.{new Date().getFullYear()}</Text></Heading>
        {/* <Text>© 2024 Your Company</Text> */}
      </Box>
      {showScroll && (
        <Box
          onClick={scrollTop}
          cursor="pointer"
          bg="white"
          borderRadius="full"
          p="2"
          // ml="4 0"
          _hover={{ bg: 'gray.200' }}
        // ml="80"
        // ml={{base:"auto", md:"auto"}}
        // mr={{base:"auto", md:"auto"}}
        >
          <Icon as={FaArrowCircleUp} boxSize="6" color="black" />
        </Box>
      )}
    </Flex>
  );
};

export default Footer;
