import React, { useState, useEffect } from "react";
import { Box, Grid, Image, Text, Icon} from "@chakra-ui/react";
import { FaHtml5, FaJs, FaReact } from "react-icons/fa";
import Shelter from "./assets/Adopt.png";
import Vault from "./assets/VaultThree.png";
import Retail from "./assets/image.png";

const projects = [
  {
    id: 1,
    name: "Instrumental Store",
    liveLink: "http://45.79.126.43",
    imageSrc: [Shelter],
    description:
      "Created a dynamic musical instrument marketplace with React for engaging interfaces, Express for robust server operations, and MongoDB for scalable data storage. Seamlessly integrated PayPal for secure transactions, enhanced by Redux for efficient state management. Featured diverse instruments, including an accordion with a customizable price slider, ensuring streamlined shopping with secure payments and intuitive navigation.",
    languageIcon: [FaHtml5,FaJs,FaReact],
  },
  {
    id: 2,
    name: "BlogPost Website",
    liveLink: "https://blog-new-frontend.onrender.com/",
    imageSrc: [Vault],
    description:
      "Developed secure MERN stack blog app with user authentication. Designed responsive UI components for CRUD on blogs/comments. Utilized React for frontend state management, Node.js/Express.js for backend API, MongoDB for efficient data storage. Demonstrated agile project management, full-stack web development skills, ensuring superb user experience with responsive design and form validation.",
    languageIcon: [FaHtml5,FaJs,FaReact],
  },
  {
    id: 3,
    name: "Ecommerce Website",
    liveLink: "https://rststore-frontend.onrender.com/",
    imageSrc: [Retail],
    description:
      "Crafted an innovative clothing ecommerce platform, harnessing React for dynamic UIs, Express for robust server-side operations, and MongoDB for scalable data storage. Seamlessly integrated with PayPal for secure transactions, enhanced by Redux for efficient state management, delivering a smooth shopping experience with secure payments and navigation.",
    languageIcon: [FaHtml5,FaJs,FaReact],
  },
  
];

const Typewriter = ({ text }) => {
  const [displayText, setDisplayText] = useState("");

  useEffect(() => {
    let currentText = "";
    let index = 0;

    const intervalId = setInterval(() => {
      currentText += text[index];
      setDisplayText(currentText);
      index++;

      if (index === text.length) {
        clearInterval(intervalId);
      }
    }, 100);

    return () => clearInterval(intervalId);
  }, [text]);

  return <Text mt="5" mb="5" align="center">{displayText}</Text>;
};

const Portfolio = () => {
  return (
    <Grid templateColumns="repeat(3, 1fr)" display={{base:"flex", md:"grid"}} flexDirection={{base:"column"}} gap={6} id="project">
      {projects.map((project) => (
        <Box key={project.id} boxShadow="md" p="4">
         <Text fontfamily="Tinos" fontSize="2xl"> 
         <Typewriter text={project.name}/></Text>
         <Box mt="2" display="flex" justifyContent="space-between" color="red">
            <a href={project.liveLink} target="_blank" rel="noopener noreferrer">Live</a>
          </Box>
          <Image src={project.imageSrc} alt="Project Image" height="300px" width="100%"/>
          <Text mt="2" height={{base:'225px', md:'180px'}} alignContent='center' width="100%">{project.description}</Text>
          <Box display="flex" alignItems="center" mt="2">
            {project.languageIcon.map((IconComponent, index) => (
              <Icon as={IconComponent} boxSize={6} key={index} ml="auto" mr="auto" />
            ))}
          </Box>
          
        </Box>
      ))}
    </Grid>
  );
};

export default Portfolio;
